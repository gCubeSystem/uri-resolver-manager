# Changelog

All notable changes to this project will be documented in this file.
This project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [v1.8.0]

- Enhanced the Geoportal-Resolver. Supported share links to Geoportal Data-Entry app [#27160]
- Provided the new endpoint: GeoportalExporter [#27308]

## [v1.7.0]

- Integrated the new Geoportal-Resolver [#24842]

## [v1.6.0] - 2022-07-26

**New**

- [#23157] Enhanced to manage the CatalogueResolver with input query string
- Moved to gcube-bom.2.0.2

## [v1.5.0] - 2021-11-05

#### Enhancement

* [22385] Integrated with Catalogue Resolver and SHUB Resolver

## [v1.4.2] - 2021-04-21

#### Bug fixes

* [#21240] Generates broken gis-link


## [v1.4.1] - 2020-05-06

* [Bug #19215] UriResolverManager: request to DL (the shortener) must be encoded


## [v1.4.0] - 2019-11-26

* Migrated to git


## [v1.3.0] - 2016-09-27

* [Bug #4941] Removed jumps of scope


## [v1.2.0] - 2016-06-28



## [v1.1.0] - 2016-06-24

* Removed maven-portal-bom as dependency


## [v1.1.0] - 2015-05-04

* Updated to support several Access Point for each Resolver

* Introduced Entry Names in Uri-Resolver-Map


## [v1.0.0] - 2014-10-13

* First Release


