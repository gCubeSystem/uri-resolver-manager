package org.gcube.portlets.user.uriresolvermanager.geoportal;

import org.gcube.portlets.user.uriresolvermanager.entity.ServiceAccessPoint;
import org.gcube.portlets.user.uriresolvermanager.readers.RuntimeResourceReader;

public class GeoportalExporterEndpoint {
	
	public final String URI_RESOLVER_RESOURCE_NAME = "HTTP-URI-Resolver";
	public final String ENTRY_POINT_NAME = "geoportal_exp";
	
	
	public GeoportalExporterEndpoint()  {
	}
	
	public ServiceAccessPoint getServiceAccessPoint()throws Exception {
		RuntimeResourceReader runtimeRR = new RuntimeResourceReader(URI_RESOLVER_RESOURCE_NAME);
		return runtimeRR.getServiceAccessPointForEntryName(ENTRY_POINT_NAME);
	}

}
