/*
 * 
 */
package org.gcube.portlets.user.uriresolvermanager.resolvers.query;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GeoportalResolverQueryStringBuilder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 27, 2023
 */
public final class GeoportalResolverQueryStringBuilder {

	public static final Logger LOG = LoggerFactory.getLogger(GeoportalResolverQueryStringBuilder.class);
	public static final String ITEM_TYPE_PARAMETER = "item_type";
	public static final String ITEM_ID_PARAMETER = "item_id";
	public static final String GCUBE_SCOPE_PARAMETER = "gcube_scope";
	public static final String RESOLVE_AS_PARAMETER = "res";
	public static final String TARGET_APP_AS_PARAMETER = "target_app";

	private String itemType;
	private String itemId;
	private String gcubeScope;
	private RESOLVE_AS resolveAs;
	private TARGET_GEOPORTAL_APP targetApp;

	/**
	 * The Enum RESOLVE_AS_PARAMETER.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Mar 28, 2023
	 */
	public static enum RESOLVE_AS {
		PUBLIC("public"), PRIVATE("private");

		String paramValue;

		/**
		 * Instantiates a new resolve as.
		 *
		 * @param paramValue the param value
		 */
		RESOLVE_AS(String paramValue) {
			this.paramValue = paramValue;
		}

		/**
		 * Gets the param value.
		 *
		 * @return the param value
		 */
		public String getParamValue() {
			return paramValue;
		}
	}

	/**
	 * The Enum TARGET_GEOPORTAL_APP.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Apr 3, 2024
	 */
	public static enum TARGET_GEOPORTAL_APP {

		GEO_DV("dv", "Geoportal Data-Viewer"), GEO_DE("de", "Geoportal Data-Entry");

		private String targetPath;
		private String applicationName;

		/**
		 * Instantiates a new target geoportal app.
		 *
		 * @param targetPath      the target path
		 * @param applicationName the application name
		 */
		private TARGET_GEOPORTAL_APP(String targetPath, String applicationName) {
			this.targetPath = targetPath;
			this.applicationName = applicationName;
		}

		/**
		 * Gets the target path.
		 *
		 * @return the target path
		 */
		public String getTargetPath() {
			return targetPath;
		}

		/**
		 * Gets the application name.
		 *
		 * @return the application name
		 */
		public String getApplicationName() {
			return applicationName;
		}

	}

	/**
	 * Instantiates a new geoportal resolver query string builder.
	 *
	 * @param itemType the item type
	 * @param itemId   the item id
	 */
	public GeoportalResolverQueryStringBuilder(String itemType, String itemId) {
		this.itemType = itemType;
		this.itemId = itemId;
	}

	/**
	 * Scope.
	 *
	 * @param gcubeScope the gcube scope
	 * @return the geoportal resolver query string builder
	 */
	public GeoportalResolverQueryStringBuilder scope(String gcubeScope) {
		this.gcubeScope = gcubeScope;
		return this;
	}

	/**
	 * Resolver as.
	 *
	 * @param resolveAs the resolve as
	 * @return the geoportal resolver query string builder
	 */
	public GeoportalResolverQueryStringBuilder resolverAs(RESOLVE_AS resolveAs) {
		this.resolveAs = resolveAs;
		return this;
	}

	/**
	 * Target app.
	 *
	 * @param targetApp the target app
	 * @return the geoportal resolver query string builder
	 */
	public GeoportalResolverQueryStringBuilder targetApp(TARGET_GEOPORTAL_APP targetApp) {
		this.targetApp = targetApp;
		return this;
	}

	/**
	 * Gets the item type.
	 *
	 * @return the item type
	 */
	public String getItemType() {
		return itemType;
	}

	/**
	 * Gets the item id.
	 *
	 * @return the item id
	 */
	public String getItemId() {
		return itemId;
	}

	/**
	 * Gets the gcube scope.
	 *
	 * @return the gcube scope
	 */
	public String getGcubeScope() {
		return gcubeScope;
	}

	/**
	 * Gets the resolve as.
	 *
	 * @return the resolve as
	 */
	public RESOLVE_AS getResolveAs() {
		return resolveAs;
	}

	/**
	 * Gets the target app.
	 *
	 * @return the target app
	 */
	public TARGET_GEOPORTAL_APP getTargetApp() {
		return targetApp;
	}

	/**
	 * Builds the query parameters.
	 *
	 * @return the map
	 */
	public Map<String, String> buildQueryParameters() {

		GeoportalResolverQueryString crQS = new GeoportalResolverQueryString(this);

		if (crQS.getItemType() == null || crQS.getItemType().isEmpty()) {
			throw new IllegalArgumentException("The " + ITEM_TYPE_PARAMETER + " cannot be null or empty");
		}

		if (crQS.getItemId() == null || crQS.getItemId().isEmpty()) {
			throw new IllegalArgumentException("The " + ITEM_ID_PARAMETER + " cannot be null or empty");
		}

		if (crQS.getGcubeScope() == null || crQS.getGcubeScope().isEmpty()) {
			throw new IllegalArgumentException("The " + GCUBE_SCOPE_PARAMETER + " cannot be null or empty");
		}

		Map<String, String> query = new HashMap<String, String>();
		query.put(ITEM_TYPE_PARAMETER, crQS.getItemType());
		query.put(ITEM_ID_PARAMETER, crQS.getItemId());
		query.put(GCUBE_SCOPE_PARAMETER, crQS.getGcubeScope());
		if (crQS.getResolveAs() != null) {
			query.put(RESOLVE_AS_PARAMETER, crQS.getResolveAs());
		}
		
		if(crQS.getTargetApp() != null) {
			query.put(TARGET_APP_AS_PARAMETER, crQS.getTargetApp());
		}

		return query;

	}

}
