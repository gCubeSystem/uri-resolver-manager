package org.gcube.portlets.user.uriresolvermanager.geoportal;

import java.net.MalformedURLException;
import java.net.URL;

import org.gcube.portlets.user.uriresolvermanager.entity.ServiceAccessPoint;
import org.gcube.portlets.user.uriresolvermanager.util.URLShortenerUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class GeoportalExporterAPI.
 * 
 * The endpoints provided by GeoportalExporter service in the URI-Resolver
 * service
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 23, 2024
 */
public class GeoportalExporterAPI {

	private GeoportalExporterEndpoint endpoint;
	private ServiceAccessPoint serviceAccessPoint;

	private final String QUERY_PARAMETER_AS_URL = "as-url";

	public static final Logger LOG = LoggerFactory.getLogger(GeoportalExporterAPI.class);

	public static String DEFAULT_TYPE = "pdf";

	/**
	 * Instantiates a new geoportal exporter API.
	 *
	 * @throws Exception the exception
	 */
	public GeoportalExporterAPI() throws Exception {
		this.endpoint = new GeoportalExporterEndpoint();
		this.serviceAccessPoint = this.endpoint.getServiceAccessPoint();
	}

	/**
	 * Export project.
	 *
	 * @param type                 the type
	 * @param ucdID                the ucd ID
	 * @param projectID            the project ID
	 * @param asDirectURLToPDFFile the as direct URL to PDF file
	 * @return the url
	 * @throws MalformedURLException the malformed URL exception
	 */
	public URL exportProject(String type, String ucdID, String projectID, boolean asDirectURLToPDFFile)
			throws MalformedURLException {
		if (type == null || type.isEmpty())
			type = DEFAULT_TYPE;

		String api = String.format("%s/export/%s/%s/%s", serviceAccessPoint.getServiceUrl(), type, ucdID, projectID);
		if (asDirectURLToPDFFile)
			api += "?" + QUERY_PARAMETER_AS_URL + "=" + asDirectURLToPDFFile;

		LOG.info("returning exportProject API: " + api);
		return new URL(api);
	}


	/**
	 * Export project.
	 *
	 * @param type the type
	 * @param ucdID the ucd ID
	 * @param projectID the project ID
	 * @param asDirectURLToPDFFile the as direct URL to PDF file
	 * @param shortLink the short link
	 * @return the string
	 * @throws MalformedURLException the malformed URL exception
	 */
	public String exportProject(String type, String ucdID, String projectID, boolean asDirectURLToPDFFile,
			boolean shortLink) throws MalformedURLException {
		URL theURL = exportProject(type, ucdID, projectID, asDirectURLToPDFFile);

		String theShortLink = null;
		if (theURL != null) {
			theShortLink = URLShortenerUtil.shortTheLink(theURL.toString());
		}

		return theShortLink;
	}

	/**
	 * View job.
	 *
	 * @param jobCode the job code
	 * @return the url
	 * @throws MalformedURLException the malformed URL exception
	 */
	public URL viewJob(String jobCode) throws MalformedURLException {
		String api = String.format("%s/view/%s", serviceAccessPoint.getServiceUrl(), jobCode);
		LOG.info("returning viewJob API: " + api);
		return new URL(api);
	}

	/**
	 * Healthcheck.
	 *
	 * @param type the type
	 * @return the url
	 * @throws MalformedURLException the malformed URL exception
	 */
	public URL healthcheck(String type) throws MalformedURLException {
		if (type == null || type.isEmpty())
			type = DEFAULT_TYPE;
		String api = String.format("%s/export/%s/healthcheck", serviceAccessPoint.getServiceUrl(), type);
		LOG.info("returning healthcheck API: " + api);
		return new URL(api);
	}

}
