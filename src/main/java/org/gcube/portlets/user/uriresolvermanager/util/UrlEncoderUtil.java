/**
 * 
 */
package org.gcube.portlets.user.uriresolvermanager.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class UrlEncoderUtil.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 13, 2014
 */
public class UrlEncoderUtil {

	public static String charset = "UTF-8";

	protected static Logger logger = LoggerFactory.getLogger(UrlEncoderUtil.class);

	/**
	 * Encode query.
	 *
	 * @param parameters the parameters
	 * @return the string
	 */
	public static String encodeQuery(String... parameters) {

		String query = "";
		for (String string : parameters) {

			try {
				query += URLEncoder.encode(string, charset) + "&";
			} catch (UnsupportedEncodingException e) {

				logger.error("encodeQuery error: ", e);
				return query;
			} catch (Exception e) {

				logger.error("encodeQuery error: ", e);
				return query;
			}

		}
		return removeLastChar(query);
	}

	/**
	 * Encode the input String using {@link URLEncoder#encode(String, String)}.
	 *
	 * @param theString the the query string
	 * @return the string encoded
	 */
	public static String encodeString(String theString) {

		String encodedQuery = "";

		if (theString == null || theString.isEmpty())
			return encodedQuery;

		try {
			encodedQuery = URLEncoder.encode(theString, charset);
		} catch (UnsupportedEncodingException e) {
			logger.error("encodeQuery error: ", e);
			return theString;
		}
		return encodedQuery;
	}

	/**
	 * To query string not encoded
	 *
	 * @param parameters the parameters
	 * @return the query string not encoded key1=value1&key2=value2&...
	 */
	public static String toQueryString(Map<String, String> parameters) {

		String query = "";

		if (parameters == null)
			return query;

		for (String key : parameters.keySet()) {

			try {

				query += String.format("%s=%s", key, parameters.get(key)) + "&";

			} catch (Exception e) {

				logger.error("getQueryString error: ", e);
				return query;
			}
		}

		return removeLastChar(query);

	}

	/**
	 * Encode query.
	 *
	 * @param parameters the parameters
	 * @return the string
	 */
	public static String encodeQuery(Map<String, String> parameters) {

		String query = "";

		if (parameters == null)
			return query;

		for (String key : parameters.keySet()) {

			try {

				query += String.format(key + "=%s", URLEncoder.encode(parameters.get(key), charset)) + "&";
			} catch (UnsupportedEncodingException e) {

				logger.error("encodeQuery error: ", e);
				return query;
			} catch (Exception e) {

				logger.error("encodeQuery error: ", e);
				return query;
			}
		}

		return removeLastChar(query);

	}

	/**
	 * Removes the last char.
	 *
	 * @param string the string
	 * @return the string
	 */
	public static String removeLastChar(String string) {

		if (string == null)
			return null;

		if (string.length() > 0)
			return string.substring(0, string.length() - 1);

		return string;
	}

	public static URI_PART getURIParts(String uri) {

		if (uri == null || uri.isEmpty())
			return null;

		String[] uriSections = uri.split("\\?");
		if (uriSections.length == 1) {
			return new URI_PART(uriSections[0], null);
		} else if (uriSections.length == 2) {
			return new URI_PART(uriSections[0], uriSections[1]);
		}

		return null;

	}

	public static class URI_PART {
		String baseURI;
		String queryString;

		public URI_PART() {
		}

		public URI_PART(String baseURI, String queryString) {
			this.baseURI = baseURI;
			this.queryString = queryString;
		}

		public String getBaseURI() {
			return baseURI;
		}

		public String getQueryString() {
			return queryString;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			builder.append("URI_PART [baseURI=");
			builder.append(baseURI);
			builder.append(", queryString=");
			builder.append(queryString);
			builder.append("]");
			return builder.toString();
		}

	}

}
