package org.gcube.portlets.user.uriresolvermanager.entity;

import java.util.Map;

/**
 * The Interface Resolver.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Nov 5, 2021
 */
public interface Resolver {

	//Code used to implement specific (so override) the method 'getLink' of the Resolver 
	public static enum RESOLVER_ENTRYNAME {
		CTLG("ctlg"), SMP("smp"), SHUB("shub"), GEO("geo"), GIS("gis");

		String entryName;

		RESOLVER_ENTRYNAME(String entryName) {
			this.entryName = entryName;
		}

		public String getEntryName() {
			return entryName;
		}
	}

	/**
	 * Gets the resource name.
	 *
	 * @return the resource name
	 */
	public String getResourceName();

	/**
	 * Gets the entry name.
	 *
	 * @return the entry name
	 */
	public String getEntryName();

	/**
	 * Gets the link.
	 *
	 * @param baseURI    the base URI
	 * @param parameters the parameters
	 * @return the link
	 * @throws Exception the exception
	 */
	public String getLink(String baseURI, Map<String, String> parameters) throws Exception;

	/**
	 * Short link.
	 *
	 * @param theLink    the the link
	 * @param parameters the parameters
	 * @return the string
	 * @throws Exception the exception
	 */
	public String shortLink(String theLink, Map<String, String> parameters) throws Exception;

}
