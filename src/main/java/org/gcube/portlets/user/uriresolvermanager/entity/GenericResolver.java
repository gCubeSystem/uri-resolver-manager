/**
 * 
 */
package org.gcube.portlets.user.uriresolvermanager.entity;

import java.util.Map;

import org.gcube.portlets.user.uriresolvermanager.exception.NotImplementedException;

/**
 * The Class Resolver.
 *
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it May 4, 2015
 */
public class GenericResolver implements Resolver {

	private String resourceName;
	private String entryName;

	/**
	 * Instantiates a new resolver.
	 *
	 * @param resourceName the resource name
	 * @param entryName    the entry name
	 */
	public GenericResolver(String resourceName, String entryName) {
		super();
		this.resourceName = resourceName;
		this.entryName = entryName;
	}

	/**
	 * Gets the resource name.
	 *
	 * @return the resourceName
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * Gets the entry name.
	 *
	 * @return the entryName
	 */
	public String getEntryName() {
		return entryName;
	}

	/**
	 * Sets the resource name.
	 *
	 * @param resourceName the resourceName to set
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * Sets the entry name.
	 *
	 * @param entryName the entryName to set
	 */
	public void setEntryName(String entryName) {
		this.entryName = entryName;
	}

	/**
	 * Gets the link.
	 *
	 * @param baseURI    the base URI
	 * @param parameters the parameters
	 * @return the link
	 * @throws Exception the exception
	 */
	@Override
	public String getLink(String baseURI, Map<String, String> parameters) throws Exception {
		throw new NotImplementedException("getLink method not implemented");
	}

	/**
	 * Short link.
	 *
	 * @param theLink    the the link
	 * @param parameters the parameters
	 * @return the string
	 * @throws Exception the exception
	 */
	@Override
	public String shortLink(String theLink, Map<String, String> parameters) throws Exception {
		throw new NotImplementedException("shortLink method not implemented");
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Resolver [resourceName=");
		builder.append(resourceName);
		builder.append(", entryName=");
		builder.append(entryName);
		builder.append("]");
		return builder.toString();
	}

}
