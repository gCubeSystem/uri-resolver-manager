package org.gcube.portlets.user.uriresolvermanager.resolvers;

import java.util.Map;

import org.gcube.portlets.user.uriresolvermanager.entity.GenericResolver;
import org.gcube.portlets.user.uriresolvermanager.util.UrlEncoderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class SHUBResolver.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 4, 2024
 */
public class SHUBResolver extends GenericResolver {

	public static final Logger LOG = LoggerFactory.getLogger(SHUBResolver.class);

	/**
	 * Instantiates a new catalogue resolver wrapper.
	 *
	 * @param resourceName the resource name
	 * @param entryName    the entry name
	 */
	public SHUBResolver(String resourceName, String entryName) {
		super(resourceName, entryName);
	}

	/**
	 * Gets the link.
	 *
	 * @param baseURI    the base URI
	 * @param parameters the parameters
	 * @return the link
	 * @throws Exception the exception
	 */
	@Override
	public String getLink(String baseURI, Map<String, String> parameters) throws Exception {
		LOG.debug("called getLink: " + baseURI + " parameters: " + parameters);
		String toReturn = null;
		try {

			String idValue = parameters.get("id");
			String pathURI = baseURI;
			if (idValue != null) {
				pathURI = String.format("%s/%s", pathURI, idValue);
				parameters.remove("id");
			}

			toReturn = pathURI;
			String queryString = UrlEncoderUtil.encodeQuery(parameters);
			if (!queryString.isEmpty())
				toReturn = String.format("%s?%s", pathURI, queryString);

		} catch (Exception e) {
			LOG.error(SHUBResolver.class.getSimpleName() + " error:  ", e);
			throw e;
		}
		LOG.info("Got Link: " + toReturn);
		return toReturn;

	}

}
