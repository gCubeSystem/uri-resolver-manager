/*
 * 
 */
package org.gcube.portlets.user.uriresolvermanager.resolvers.query;

import java.util.HashMap;
import java.util.Map;

import org.gcube.portlets.user.uriresolvermanager.resolvers.query.CatalogueResolverQueryString.MODERATION_OP;
import org.gcube.portlets.user.uriresolvermanager.util.UrlEncoderUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class CatalogueResolverQueryStringBuilder.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 26, 2022
 */
public final class CatalogueResolverQueryStringBuilder {

	// DEFAULT PARAMETERS
	public static final String DEFAULT_STATUS = "pending";
	public static final String DEFAULT_MODERATION_OP = MODERATION_OP.show.name();

	public static final Logger LOG = LoggerFactory.getLogger(CatalogueResolverQueryStringBuilder.class);
	public static final String MODERATION_PARAMETER = "moderation";
	public static final String ITEM_NAME_PARAMETER = "item_name";
	public static final String ITEM_ID_PARAMETER = "item_id";
	public static final String STATUS_PARAMETER = "status";

	public static final String QUERY_STRING_PARAMETER = "query_string";

	private final String itemName;
	private String itemId;
	private String itemStatus;
	private MODERATION_OP moderation;

	/**
	 * Instantiates a new catalogue resolver query string builder.
	 *
	 * @param itemName the item name
	 */
	public CatalogueResolverQueryStringBuilder(String itemName) {
		this.itemName = itemName;
	}

	/**
	 * Item id.
	 *
	 * @param itemId the item id
	 * @return the catalogue resolver query string builder
	 */
	public CatalogueResolverQueryStringBuilder itemId(String itemId) {
		this.itemId = itemId;
		return this;
	}

	/**
	 * Moderation.
	 *
	 * @param moderation the moderation
	 * @return the catalogue resolver query string builder
	 */
	public CatalogueResolverQueryStringBuilder moderation(MODERATION_OP moderation) {
		this.moderation = moderation;
		return this;
	}

	/**
	 * Item status.
	 *
	 * @param itemStatus the item status
	 * @return the catalogue resolver query string builder
	 */
	public CatalogueResolverQueryStringBuilder itemStatus(String itemStatus) {
		this.itemStatus = itemStatus;
		return this;
	}

	/**
	 * Gets the item name.
	 *
	 * @return the item name
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * Gets the item id.
	 *
	 * @return the item id
	 */
	public String getItemId() {
		return itemId;
	}

	/**
	 * Gets the item status.
	 *
	 * @return the item status
	 */
	public String getItemStatus() {
		return itemStatus;
	}

	/**
	 * Gets the moderation.
	 *
	 * @return the moderation
	 */
	public MODERATION_OP getModeration() {
		return moderation;
	}

	/**
	 * Builds the query parameters.
	 *
	 * @return the map
	 */
	public Map<String, String> buildQueryParameters() {

		CatalogueResolverQueryString crQS = new CatalogueResolverQueryString(this);

		if (crQS.getItemName() == null || crQS.getItemName().isEmpty()) {
			throw new IllegalArgumentException("The " + ITEM_NAME_PARAMETER + " cannot be null or empty");
		}

		Map<String, String> query = new HashMap<String, String>();
		query.put(ITEM_NAME_PARAMETER, crQS.getItemName());

		if (crQS.getItemId() != null) {
			query.put(ITEM_ID_PARAMETER, crQS.getItemId());
		}

		if (crQS.getItemStatus() != null) {
			query.put(STATUS_PARAMETER, crQS.getItemStatus());
		} else {
			query.put(STATUS_PARAMETER, DEFAULT_STATUS);
		}

		if (crQS.getModeration() != null) {
			query.put(MODERATION_PARAMETER, crQS.getModeration().name());
		} else {
			query.put(MODERATION_PARAMETER, DEFAULT_MODERATION_OP);
		}

		return query;

	}

	/**
	 * Builds the query parameters to query string.
	 *
	 * @return the string
	 */
	public String buildQueryParametersToQueryString() {

		Map<String, String> mapParameters = buildQueryParameters();
		return UrlEncoderUtil.toQueryString(mapParameters);
	}

	/**
	 * Builds the query obj.
	 *
	 * @return the catalogue resolver query string
	 */
	public CatalogueResolverQueryString buildQueryObj() {

		return new CatalogueResolverQueryString(this);

	}

}
