package org.gcube.portlets.user.uriresolvermanager.resolvers.query;

/**
 * The Class GeoportalResolverQueryString.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Mar 27, 2023
 */
public class GeoportalResolverQueryString {

	private final String itemType;
	private String itemId;
	private String gcubeScope;
	private String resolveAs;
	private String targetApp;

	/**
	 * Instantiates a new geoportal resolver query string.
	 *
	 * @param builder the builder
	 */
	public GeoportalResolverQueryString(GeoportalResolverQueryStringBuilder builder) {
		this.itemType = builder.getItemType();
		this.itemId = builder.getItemId();
		this.gcubeScope = builder.getGcubeScope();
		this.resolveAs = builder.getResolveAs() != null ? builder.getResolveAs().getParamValue() : null;
		this.targetApp = builder.getTargetApp() != null ? builder.getTargetApp().getTargetPath() : null;

	}

	/**
	 * Gets the item type.
	 *
	 * @return the item type
	 */
	public String getItemType() {
		return itemType;
	}

	/**
	 * Gets the item id.
	 *
	 * @return the item id
	 */
	public String getItemId() {
		return itemId;
	}

	/**
	 * Gets the gcube scope.
	 *
	 * @return the gcube scope
	 */
	public String getGcubeScope() {
		return gcubeScope;
	}

	/**
	 * Gets the resolver as.
	 *
	 * @return the resolver as
	 */
	public String getResolveAs() {
		return resolveAs;
	}

	/**
	 * Gets the target app.
	 *
	 * @return the target app
	 */
	public String getTargetApp() {
		return targetApp;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("GeoportalResolverQueryString [itemType=");
		builder.append(itemType);
		builder.append(", itemId=");
		builder.append(itemId);
		builder.append(", gcubeScope=");
		builder.append(gcubeScope);
		builder.append(", resolveAs=");
		builder.append(resolveAs);
		builder.append(", targetApp=");
		builder.append(targetApp);
		builder.append("]");
		return builder.toString();
	}

}
