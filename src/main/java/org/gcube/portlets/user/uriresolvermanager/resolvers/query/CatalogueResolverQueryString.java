package org.gcube.portlets.user.uriresolvermanager.resolvers.query;

/**
 * The Class CatalogueResolverQueryString.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 *         Apr 26, 2022
 */
public class CatalogueResolverQueryString {

	/**
	 * The Enum MODERATION_OP.
	 *
	 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
	 * 
	 *         Apr 26, 2022
	 */
	public static enum MODERATION_OP {
		show
	}

	private final String itemName;
	private String itemId;
	private String itemStatus;
	private MODERATION_OP moderation;

	/**
	 * Instantiates a new catalogue resolver query string.
	 *
	 * @param builder the builder
	 */
	CatalogueResolverQueryString(CatalogueResolverQueryStringBuilder builder) {
		this.itemName = builder.getItemName();
		this.itemId = builder.getItemId();
		this.itemStatus = builder.getItemStatus();
		this.moderation = builder.getModeration();

	}

	/**
	 * Gets the item id.
	 *
	 * @return the item id
	 */
	public String getItemId() {
		return itemId;
	}

	/**
	 * Gets the moderation.
	 *
	 * @return the moderation
	 */
	public MODERATION_OP getModeration() {
		return moderation;
	}

	/**
	 * Gets the item name.
	 *
	 * @return the item name
	 */
	public String getItemName() {
		return itemName;
	}

	/**
	 * Gets the item status.
	 *
	 * @return the item status
	 */
	public String getItemStatus() {
		return itemStatus;
	}

	/**
	 * To string.
	 *
	 * @return the string
	 */
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("CatalogueResolverQueryString [itemName=");
		builder.append(itemName);
		builder.append(", itemId=");
		builder.append(itemId);
		builder.append(", itemStatus=");
		builder.append(itemStatus);
		builder.append(", moderation=");
		builder.append(moderation);
		builder.append("]");
		return builder.toString();
	}

}
