package org.gcube.portlets.user.uriresolvermanager.exception;

/**
 * The Class NotImplementedException.
 *
 * @author Francesco Mangiacrapa at ISTI-CNR francesco.mangiacrapa@isti.cnr.it
 * 
 * Nov 5, 2021
 */
public class NotImplementedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7350078577553202213L;

	/**
	 * Instantiates a new not implemented exception.
	 */
	public NotImplementedException() {
	}

	/**
	 * Instantiates a new not implemented exception.
	 *
	 * @param message the message
	 */
	public NotImplementedException(String message) {
		super(message);
	}

}
