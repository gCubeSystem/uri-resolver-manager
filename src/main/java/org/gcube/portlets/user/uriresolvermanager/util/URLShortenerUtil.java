package org.gcube.portlets.user.uriresolvermanager.util;

import org.gcube.portlets.user.urlshortener.UrlShortener;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class URLShortenerUtil {

	private static Logger LOG = LoggerFactory.getLogger(URLShortenerUtil.class);
	
	/**
	 * Short the link.
	 *
	 * @param link the link
	 * @return the string
	 */
	public static String shortTheLink(String link) {

		String toReturnLink = link;
		try {
			UrlShortener shortener = new UrlShortener();
			String shortedLink = shortener.shorten(link);
			LOG.info("Short link is: " + shortedLink);
			toReturnLink = shortedLink;
		} catch (Exception e) {
			LOG.warn("Returning source link, an error occurred during link shortening: ", e);
		}
		return toReturnLink;
	}
}
