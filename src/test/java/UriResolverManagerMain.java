import org.gcube.portlets.user.uriresolvermanager.util.UrlEncoderUtil;

/**
 *
 */

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 20, 2014
 *
 */
public class UriResolverManagerMain {

	public static void main(String[] args) {
		try {
			
			String theLink = "https://data.dev.d4science.org/ctlg/devVRE/sarda-sarda";
			String[] queryStringArray = theLink.split("\\?");
			if(queryStringArray.length>1) {
				String queryString = queryStringArray[1];
				String queryStringEncoded = UrlEncoderUtil.encodeQuery(queryString);
				theLink = String.format("%s?%s", queryStringArray[0], queryStringEncoded);
			}
			
			System.out.println(theLink);
			
//			ScopeProvider.instance.set("/gcube");
//			UriResolverManager resolver = new UriResolverManager("GIS");
//			System.out.println(resolver.getCapabilities());
//			System.out.println(resolver.getApplicationTypes());
//			System.out.println(resolver.discoveryServiceParameters(resolver.getResolver("SMP-ID")));

//			Map<String, String> params = new HashMap<String, String>();
//			params.put("gis-UUID", "5ac49f44-999f-4efe-a32b-af71da2b39ac");
//			params.put("scope", "/gcube/devsec/devVRE");
//			String shortLink = resolver.getLink(params, true);
////			System.out.println(shortLink); //true, link is shorted otherwise none
//		} catch (UriResolverMapException e) {
//			e.printStackTrace();
//		} catch (IllegalArgumentException e) {
//			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * The main method.
	 *
	 * @param args the arguments
	 */
//	public static void main(String[] args) {
//
////		System.out.println(UrlEncoderUtil.encodeQuery("request=GetStyles", "layers=test Name", "service=WMS", "version=1.1.1"));
//
//		HashMap<String, String> parameters = new HashMap<String, String>();
//
//		parameters.put("request", "GetStyles");
//		parameters.put("layers", "test Name");
//		parameters.put("version", "1.1.1");
//
//		System.out.println(UrlEncoderUtil.encodeQuery(parameters));
//
//	}

}
