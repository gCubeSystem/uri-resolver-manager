import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.uriresolvermanager.UriResolverManager;
import org.gcube.portlets.user.uriresolvermanager.entity.Resolver;
import org.gcube.portlets.user.uriresolvermanager.entity.ServiceParameter;
import org.gcube.portlets.user.uriresolvermanager.exception.IllegalArgumentException;
import org.gcube.portlets.user.uriresolvermanager.exception.UriResolverMapException;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.CatalogueResolverQueryStringBuilder;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder.RESOLVE_AS;
import org.gcube.portlets.user.uriresolvermanager.resolvers.query.GeoportalResolverQueryStringBuilder.TARGET_GEOPORTAL_APP;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 20, 2014
 *
 */
public class UriResolverManagerTest {

	//@Test
	public void testUriResolverManger() {
		UriResolverManager manager;
		try {
			ScopeProvider.instance.set("/gcube/devsec/devVRE");
			manager = new UriResolverManager();
			System.out.println("Capabilities: " + manager.getCapabilities());
			System.out.println("ApplicationTypes: " + manager.getApplicationTypes());
			for (String applicationType : manager.getApplicationTypes()) {
				Resolver resolver = manager.getResolver(applicationType);
				System.out.println("ApplicationType: " + applicationType + " has: " + resolver);
				List<ServiceParameter> serviceParameters = manager.discoveryServiceParameters(resolver);
				System.out.println("Parameters: " + serviceParameters);
			}
		} catch (UriResolverMapException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	//@Test
	public void testCTLG() {

		try {
			ScopeProvider.instance.set("/gcube/devsec/devVRE");
			UriResolverManager resolver;
			resolver = new UriResolverManager("CTLG");
			Map<String, String> params = new HashMap<String, String>();
			params.put("gcube_scope", "/gcube/devsec/devVRE");
			params.put("entity_context", "dataset");
			params.put("entity_name", "sarda-sarda");
			String shortLink = resolver.getLink(params, true);
			System.out.println(shortLink);
		} catch (UriResolverMapException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//@Test
	public void testCTLGWithQueryString() {

		try {
			ScopeProvider.instance.set("/gcube/devsec/devVRE");
			UriResolverManager resolver;
			resolver = new UriResolverManager("CTLG");
			Map<String, String> params = new HashMap<String, String>();
			params.put("gcube_scope", "/gcube/devsec/devVRE");
			params.put("entity_context", "dataset");
			params.put("entity_name", "sarda-sarda");
			params.put("query_string", "param1=value1&parm2=value2");
			// METHOD 1 - Query String as parameter of the getLink method
			String shortLink = resolver.getLink(params, true);

			// METHOD 2 - Query String as parameter passed in the params
//			String queryString = QueryStringUtil.toQueryString(queryStringParameters);
//			params.put(CatalogueResolverQueryStringBuilder.QUERY_STRING_PARAMETER, queryString);
			System.out.println(shortLink);
		} catch (UriResolverMapException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	//@Test
	public void testCTLGGenerateLinkForModeration() {

		try {
			ScopeProvider.instance.set("/gcube/devsec/devVRE");
			UriResolverManager resolver;
			resolver = new UriResolverManager("CTLG");
			Map<String, String> params = new HashMap<String, String>();
			params.put("gcube_scope", "/gcube/devsec/devVRE");
			params.put("entity_context", "organization");
			params.put("entity_name", "devvre");

			CatalogueResolverQueryStringBuilder builder = new CatalogueResolverQueryStringBuilder(
					"test-moderation-1649068829317");
			//builder.itemStatus("approved").moderation(MODERATION_OP.show);

			String queryString = builder.buildQueryParametersToQueryString();
			params.put(CatalogueResolverQueryStringBuilder.QUERY_STRING_PARAMETER, queryString);

			String shortLink = resolver.getLink(params, true);
			System.out.println(shortLink);
		} catch (UriResolverMapException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	//@Test
	public void testGeoportalCreateDataEntryLink() {

		try {
			String scope = "/gcube/devsec/devVRE";
//			String scope = "/pred4s/preprod/preVRE";
			ScopeProvider.instance.set(scope);
			String gcubeScope = scope;
			String itemId = "6384aaac308f5c28c5ee0888";
			String itemType = "profiledConcessioni";
			
			UriResolverManager resolver = new UriResolverManager("GEO");
			Map<String, String> params = null;
			
			//Method 1
//			params = new HashMap<String, String>();
//			params.put(GeoportalResolverQueryStringBuilder.GCUBE_SCOPE_PARAMETER, gcubeScope);
//			params.put(GeoportalResolverQueryStringBuilder.ITEM_ID_PARAMETER, itemId);
//			params.put(GeoportalResolverQueryStringBuilder.ITEM_TYPE_PARAMETER, itemType);
//			params.put(GeoportalResolverQueryStringBuilder.RESOLVE_AS_PARAMETER, GeoportalResolverQueryStringBuilder.RESOLVE_AS.PUBLIC.getParamValue());

			//Method 2
			GeoportalResolverQueryStringBuilder builder = new GeoportalResolverQueryStringBuilder(itemType,itemId);
			builder.scope(gcubeScope);
			builder.resolverAs(RESOLVE_AS.PUBLIC);
			builder.targetApp(TARGET_GEOPORTAL_APP.GEO_DE);
//			builder.resolverAs(RESOLVE_AS.PRIVATE);
			params = builder.buildQueryParameters();

			String shortLink = resolver.getLink(params, true);
			System.out.println(shortLink);
		} catch (UriResolverMapException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	//@Test
	public void testGeoportalCreateDataViewerLink() {

		try {
			String scope = "/gcube/devsec/devVRE";
//			String scope = "/pred4s/preprod/preVRE";
			ScopeProvider.instance.set(scope);
			String gcubeScope = scope;
			String itemId = "6384aaac308f5c28c5ee0888";
			String itemType = "profiledConcessioni";
			
			UriResolverManager resolver = new UriResolverManager("GEO");
			Map<String, String> params = null;
			
			//Method 1
//			params = new HashMap<String, String>();
//			params.put(GeoportalResolverQueryStringBuilder.GCUBE_SCOPE_PARAMETER, gcubeScope);
//			params.put(GeoportalResolverQueryStringBuilder.ITEM_ID_PARAMETER, itemId);
//			params.put(GeoportalResolverQueryStringBuilder.ITEM_TYPE_PARAMETER, itemType);
//			params.put(GeoportalResolverQueryStringBuilder.RESOLVE_AS_PARAMETER, GeoportalResolverQueryStringBuilder.RESOLVE_AS.PUBLIC.getParamValue());

			//Method 2
			GeoportalResolverQueryStringBuilder builder = new GeoportalResolverQueryStringBuilder(itemType,itemId);
			builder.scope(gcubeScope);
			builder.resolverAs(RESOLVE_AS.PUBLIC);
			//builder.targetApp(TARGET_GEOPORTAL_APP.GEO_DV);
			builder.resolverAs(RESOLVE_AS.PRIVATE);
			params = builder.buildQueryParameters();

			String shortLink = resolver.getLink(params, true);
			System.out.println(shortLink);
		} catch (UriResolverMapException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// @Test IS OK
	public void testSHUB() {

		try {
			ScopeProvider.instance.set("/gcube/devsec/devVRE");
			UriResolverManager resolver;
			resolver = new UriResolverManager("SHUB");
			Map<String, String> params = new HashMap<String, String>();
			params.put("id", "1dac6703-8eb0-4838-83a8-5006f5074e9b");
			params.put("content-disposition", "inline");
			String shortLink = resolver.getLink(params, true);
			System.out.println(shortLink);
		} catch (UriResolverMapException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// @Test
	public void testGIS() {

		try {
			String scope = "/pred4s/preprod/preVRE";
			String UUID = "da165110-88fd-11da-a88f-000d939bc5d8";
			ScopeProvider.instance.set(scope);
			UriResolverManager resolver = new UriResolverManager("GIS");
			Map<String, String> params = new HashMap<String, String>();
			params.put("gis-UUID", UUID);
			params.put("scope", scope);
			String shortLink = resolver.getLink(params, true);
			System.out.println(shortLink);
		} catch (UriResolverMapException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// @Test
	public void testSMP() {

		try {
			ScopeProvider.instance.set("/gcube/devsec/devVRE");
			UriResolverManager resolver = new UriResolverManager("SMP");
			Map<String, String> params = new HashMap<String, String>();
			params.put("smp-uri",
					"smp://Wikipedia_logo_silver.png?5ezvFfBOLqaqBlwCEtAvz4ch5BUu1ag3yftpCvV gayz9bAtSsnO1/sX6pemTKbDe0qbchLexXeWgGcJlskYE8td9QSDXSZj5VSl9kdN9SN0/LRYaWUZuP4Q1J7lEiwkU4GKPsiD6PDRVcT4QAqTEy5hSIbr6o4Y");
			params.put("fileName", "wikipediaLogo");
			params.put("contentType", "");
			String shortLink = resolver.getLink(params, true); // true, link is shorted otherwise none
			System.out.println(shortLink);
		} catch (UriResolverMapException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Thread safe
	 */
	// @Test
	public void testSMPID() {

		try {
			ScopeProvider.instance.set("/gcube/devsec/devVRE");
			UriResolverManager resolver;
			resolver = new UriResolverManager("SMP-ID");
			Map<String, String> params = new HashMap<String, String>();
			params.put("geo-exp", "553f9265e4b0567b75021fce");
//			params.put("fileName", "dog");
//			params.put("contentType", "image/jpg");
			String shortLink = resolver.getLink(params, true); // true, link is shorted otherwise none
			System.out.println(shortLink);
		} catch (UriResolverMapException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Thread safe
	 */
	// @Test
	public void test2() {

		// create thread to print counter value
		Thread t = new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {

						ScopeProvider.instance.set("/gcube/devsec/devVRE");
						UriResolverManager resolver;
						resolver = new UriResolverManager("GIS");

						Map<String, String> params = new HashMap<String, String>();
						params.put("gis-UUID", "eb1a1b63-f324-47ee-9522-b8f5803e19ec");
						params.put("scope", "/gcube/devsec/devVRE");
						String shortLink = resolver.getLink(params, true);
						System.out.println(shortLink); // true, link is shorted otherwise none

						System.out.println("Thread " + Thread.currentThread().getId() + " reading counter is: "
								+ resolver.countReaders());
						Thread.sleep(1000);
					} catch (InterruptedException ex) {
						ex.printStackTrace();
					} catch (UriResolverMapException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}

		});

		t.start();

		try {
			Thread.sleep(1000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

}
