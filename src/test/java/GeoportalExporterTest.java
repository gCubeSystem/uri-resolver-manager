import java.net.URL;

import org.gcube.common.scope.api.ScopeProvider;
import org.gcube.portlets.user.uriresolvermanager.UriResolverManager;
import org.gcube.portlets.user.uriresolvermanager.exception.IllegalArgumentException;
import org.gcube.portlets.user.uriresolvermanager.exception.UriResolverMapException;
import org.gcube.portlets.user.uriresolvermanager.geoportal.GeoportalExporterAPI;

/**
 * @author Francesco Mangiacrapa francesco.mangiacrapa@isti.cnr.it
 * @Oct 20, 2014
 *
 */
public class GeoportalExporterTest {

	//@Test
	public void testGeoportalExporterEndppoint() {
		UriResolverManager manager;
		try {
			ScopeProvider.instance.set("/gcube/devsec/devVRE");
			GeoportalExporterAPI geAPI = new GeoportalExporterAPI();
			
			boolean asURL = true;
			URL exportProjectURL = geAPI.exportProject("pdf", "profiledConcessioni", "661d2c6f8804530afb90b132", asURL);
			System.out.println("exportProjectURL: "+exportProjectURL);
			URL healthcheckURL = geAPI.healthcheck("pdf");
			System.out.println("healthcheckURL: "+healthcheckURL);

			
		} catch (UriResolverMapException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
}
